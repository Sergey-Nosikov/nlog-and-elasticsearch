﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using ApiWithLog.Services.HttpLogger.HttpLogReader;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace ApiWithLog.Controllers
{
	[Route("api/[controller]")]
	[ApiController]
	public class LogController : Controller
	{
		private HttpLogReaderService _logInfo { get; }

		public LogController(HttpLogReaderService logInfo)
		{
			_logInfo = logInfo;
		}

		[HttpGet]
		public IActionResult Log(int from = 0, int size = 10, DateTime? startDate = null, DateTime? endDate = null, string userName = null,
			int? statusCode = null, string path = null)
		{
			try
			{
				var seatchResult = _logInfo.Search(from, size, startDate, endDate, userName, statusCode, path);
				
				return new JsonResult(new
				{
					status = seatchResult.Count() == 0 ? "Results is empty" : "OK",
					result = seatchResult
				});
			}
			catch (Exception e)
			{
				return new JsonResult(new
				{
					status = e.GetType().ToString(),
					result = e.Message
				});
			}

		}
	}
}
