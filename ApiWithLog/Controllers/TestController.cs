﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace ApiWithLog.Controllers
{
	[Route("api/[controller]")]
	[ApiController]
	public class TestController : Controller
	{

		[HttpGet]
		public IActionResult Test()
		{
			return new JsonResult(
				new { 
					status = "OK",
					value = "Test" 
				}
			);
		}
	}
}
