﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace ApiWithLog.Middlewares.HttpLogger
{
	public class BodyBuffering
	{
		private readonly RequestDelegate _next;

		public BodyBuffering(RequestDelegate next)
		{
			_next = next;
		}

		public async Task InvokeAsync(HttpContext context)
		{
			context.Request.EnableBuffering();

			var originalResponseBody = context.Response.Body;
			var fakeResponseBody = new MemoryStream();
			try
			{
				context.Response.Body = fakeResponseBody;
				await _next(context);
			}
			finally
			{
				await fakeResponseBody.CopyToAsync(originalResponseBody);
				fakeResponseBody.Close();
				fakeResponseBody.Dispose();
				context.Response.Body = originalResponseBody;
			}
		}
	}
}
