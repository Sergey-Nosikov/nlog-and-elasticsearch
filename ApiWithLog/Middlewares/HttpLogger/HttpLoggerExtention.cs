﻿using ApiWithLog.HttpLogger.Middlewares;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiWithLog.Middlewares.HttpLogger
{
	public static class HttpLoggerExtension
	{
		public static IApplicationBuilder UseHttpLogger(this IApplicationBuilder app) => app
			.UseMiddleware<BodyBuffering>()
			.UseMiddleware<HttpLoggerMiddleware>();
	}
}
