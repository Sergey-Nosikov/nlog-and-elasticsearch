﻿using ApiWithLog.Services.HttpLogger;
using ApiWithLog.Services.HttpLogger.HttpLogWriter;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.WebUtilities;
using Microsoft.VisualBasic;
using Nest;
using NLog;
using NLog.Fluent;
using NLog.Targets.Wrappers;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Mime;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace ApiWithLog.HttpLogger.Middlewares
{
	public class HttpLoggerMiddleware
	{
		private readonly HttpLogWriterService _httpLogWriterService;
		private readonly RequestDelegate _next;
		public HttpLoggerMiddleware(RequestDelegate next, HttpLogWriterService httpLogWriterService)
		{
			this._next = next;
			this._httpLogWriterService = httpLogWriterService;
		}

		public async Task InvokeAsync(HttpContext context)
		{
			var logMessage = _httpLogWriterService.CreateLogMessage();

			logMessage.Protocol = context.Request.Protocol;
			logMessage.Method = context.Request.Method;
			logMessage.Path = context.Request.Path;
			logMessage.Query = context.Request.QueryString.Value;

			logMessage.IsAuthenticated = context.User.Identity.IsAuthenticated.ToString();
			logMessage.UserName = context.User.Identity.Name;

			logMessage.RequestContentType = context.Request.ContentType;
			logMessage.RequestBody = await ReadBodyAsync(context.Request.Body, context.Request.ContentType);

			await _next(context);

			logMessage.ResponseStatusCode = context.Response.StatusCode.ToString();
			logMessage.ResponseContentType = context.Response.ContentType;
			logMessage.ResponseBody = await ReadBodyAsync(context.Response.Body, context.Response.ContentType);

			_httpLogWriterService.Write(logMessage, "Something message");
		}

		private async Task<string> ReadBodyAsync(Stream body, string contentType)
		{
			if (string.IsNullOrEmpty(contentType)) return null;

			var contType = new ContentType(contentType);
			var isValid = ValidTypes.Any(t => t == contType.MediaType);

			if (!isValid) return null;

			var charSet = contType.Parameters["charset"];
			var encoding = string.IsNullOrEmpty(charSet) ? Encoding.Default : Encoding.GetEncoding(charSet);

			body.Position = 0;
			var responseBody = new StreamReader(body).ReadToEndAsync();
			body.Position = 0;

			return await responseBody;
		}

		private string[] ValidTypes = new string[] {
			"application/json"
		};
	}
}