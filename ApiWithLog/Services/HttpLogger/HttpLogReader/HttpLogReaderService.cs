﻿using Nest;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ApiWithLog.Services.HttpLogger.HttpLogReader
{
	public class HttpLogReaderService
	{
		private IElasticClient eClient { get; }
		public HttpLogReaderService(IElasticClient elasticClient)
		{
			eClient = elasticClient;
		}

		public ItemLog GetById(string id) =>
			throw new NotImplementedException();

		public IEnumerable<ItemLog> Search(
			int from = 0,
			int size = 10,
			DateTime? startDate = null,
			DateTime? endDate = null,
			string userName = null,
			int? statusCode = null,
			string path = null)
		{
			var result = eClient.Search<ItemLog>(s => s
			   .Index("httplog")
			   .Pretty()
			   .From(from)
			   .Size(size)
			   .Query(q => q
				   .Bool(b => b
					   .Must(mu => mu
						   .Match(m => m
							   .Field("Path")
							   .Query(path)
						   ),
							mu => mu
							.Match(m => m
								.Field("UserName")
								.Query(userName)
							),
							mu => mu
							.Match(m => m
								.Field("StatusCode")
								.Query(statusCode.ToString())
							)
					   )
					   .Filter(fi => fi
						   .DateRange(r => r
							   .Field("@timestamp")
							   .GreaterThan(startDate)
							   .LessThan(endDate)
						   )
					   )
				   )
			   )
			);

			return result.HitsMetadata.Hits.Select(h => h.Source);
		}
	}
}
