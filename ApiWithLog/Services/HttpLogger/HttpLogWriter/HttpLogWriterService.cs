﻿using NLog;

namespace ApiWithLog.Services.HttpLogger.HttpLogWriter
{
	public class HttpLogWriterService
	{
		private Logger _logger { get; } = LogManager.GetLogger("httplog");
		public IHttpLogMessage CreateLogMessage() => new ItemLog();

		public void Write(IHttpLogMessage logMessage, string message)
		{
			var logEventInfo = new LogEventInfo(LogLevel.Info, "", message);
			var properties = typeof(IHttpLogMessage).GetProperties();

			foreach (var prop in properties) 
				logEventInfo.Properties.Add(prop.Name, prop.GetValue(logMessage));

			_logger.Info(logEventInfo);
		}

	}
}
