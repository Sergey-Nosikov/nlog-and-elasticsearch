﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiWithLog.Services.HttpLogger
{
	public class ItemLog : IHttpLogMessage
	{
		public DateTime Timestamp { get; set; }
		public string Time { get; set; }
		public string Level { get; set; }
		public string Message { get; set; }

		public string Protocol { get; set; }
		public string Method { get; set; }
		public string Path { get; set; }
		public string Query { get; set; }

		public string IsAuthenticated { get; set; }
		public string UserName { get; set; }

		public string RequestContentType { get; set; }
		public string RequestBody { get; set; }
		public string ResponseContentType { get; set; }
		public string ResponseBody { get; set; }
		public string ResponseStatusCode { get; set; }
	}
}