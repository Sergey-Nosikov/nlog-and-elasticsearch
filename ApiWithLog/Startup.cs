using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ApiWithLog.HttpLogger.Middlewares;
using ApiWithLog.Middlewares.HttpLogger;
using ApiWithLog.Services.HttpLogger.HttpLogReader;
using ApiWithLog.Services.HttpLogger.HttpLogWriter;
using Elasticsearch.Net;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Logging.Configuration;
using Nest;
using NLog.Extensions.Logging;

namespace ApiWithLog
{
	public class Startup
	{
		public Startup(IConfiguration configuration)
		{
			Configuration = configuration;
		}

		public IConfiguration Configuration { get; }

		public void ConfigureServices(IServiceCollection services)
		{
			services.AddControllers();
			services.AddTransient<HttpLogReaderService>();
			services.AddTransient<HttpLogWriterService>();
			services.AddTransient<IElasticClient, ElasticClient>(serviceProvider =>
			{
				var connectionsString = new[]{
					Configuration.GetConnectionString("ElasticSearchServerAddress")
					//  ...
				};

				var connectionPool = new StaticConnectionPool(
					connectionsString.Select(conn => new Uri(conn))
				);

				var settings = new ConnectionSettings(connectionPool);

				return new ElasticClient(settings);
			});
		}

		public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
		{
			app.UseHttpLogger();

			if (env.IsDevelopment())
				app.UseDeveloperExceptionPage();

			app.UseRouting();
			app.UseEndpoints(endpoints =>
			{
				endpoints.MapControllers();
			});
		}
	}
}
